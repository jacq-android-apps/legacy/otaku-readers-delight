# Otaku Reader's Delight
Android BookListing App I original created in 2017.

### 🗒️Overview

A book listing app that allows a user to get a list of the first 20 published books on a given topic.
The app will connect to the Google Books API in order to fetch results and displays them.
```
- Using an AsyncTask
- Parsing a JSON response
- Checks for internet connection
- Using ListView and Adapter to populate a list.
```

### 🏁 Getting Started

#### Prerequisites

To successfully build and run this app locally in Android Studio, you need to following:

| Config       | Details                   |
|--------------|---------------------------|
| Android SDK  | 8.0 (Oreo) API 26         |
| Gradle /     | v7.3.3                    |
| $agp_version | v7.2.0                    |
| Gradle JDK   | Jetbrains runtime v17.0.7 |

In file `./build.gradle` *buildscript.repositories* & *allprojects.repositories* contain:
```
google()
mavenCentral()
```

### 📷 Screenshots

|                         Empty State                          |                          Query Result                           |                            No Result                            |
|:------------------------------------------------------------:|:---------------------------------------------------------------:|:---------------------------------------------------------------:|
| ![Empty state](public/screens/empty-state.webp){width=300px} | ![Query result](public/screens/query-results.webp){width=300px} | ![Empty result](public/screens/empty-results.webp){width=300px} |

|                           No Internet Dialog                            |                                Landscape                                |
|:-----------------------------------------------------------------------:|:-----------------------------------------------------------------------:|
| ![No Internet](public/screens/no-internet-connection.webp){width=300px} | ![Edit product](public/screens/landscape-orientation.webp){width=300px} |

_Disclaimer: None of the images used to create this app belong to me._
